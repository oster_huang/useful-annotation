
// const defaultCatchType = "log";

const dispenseCatch = err => {
  alert('Exception Catched...' + err)
};

/**
 * 可以帮助你自动捕获 async 函数中的错误，不需要额外的 then catch 或是 try catch
 *
 * 只能用在vue component中
 */ 
export default () => (target, name, descriptor) => {
  const _copy = descriptor.value;

  descriptor.value = function(...args) {
    return Promise.resolve(_copy.apply(this, args)).catch(
      dispenseCatch.bind(this)
    );
  };
};
