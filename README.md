# useful-annotation

Decorator
annotation風格，讓function不用寫程式碼，就可以辦到其他的事情。

` 此頁面為 vue-cli3 建立之專案 `

#### 必要條件
必須支援 es7 規格 - 建議使用vue-cli3

#### Example
在需要擴充的js function上方，加入@開頭的annotation

```javascript
/**
 * 確保該function在時間內，只會執行一次
 */
@Debounce()
function handleClick() {
    this.count += 1
}
 
/**
 * 取得uncatched exception做統一處理
 */
@AutoCatch()
async function handleSubmit() {
    .....
}

```

## 執行方式說明
### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
